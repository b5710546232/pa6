package creditTransfer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Searcher is class that use for search information
 * 
 * @author nattapat
 * @author kundjanasith
 *
 */
public class Searcher {
	
	/**
	 * select is String that search from type
	 */
	private String select;
	
	/**
	 * key is String that search for find key
	 */
	private String key;
	
	/**
	 * fromUniversity is String that name of University 
	 * that user come from
	 */
	private String fromUniversity;
	
	/**
	 * fromFaculty is String that name of Faculty
	 * that user come from
	 */
	private String fromFaculty;
	
	/**
	 * fromMajor is String that name of Major
	 * that user come from
	 */
	private String fromMajor;
	
	/**
	 * toUniversity is String that name of University
	 * that user will go to
	 */
	private String toUniversity;
	
	/**
	 * toFaculty is String that name of Faculty
	 * that user will go to
	 */
	private String toFaculty;
	
	/**
	 * toMajor is String that name of Major 
	 * that user will go to
	 */
	private String toMajor;
    
	/**
	 * fromSubject is ArrayList<String> that collect every subject-id
	 * that user come from
	 */
	private ArrayList<String> fromSubject;
	
	/**
	 * toSubject is ArrayList<String> that collect every subject-id
	 * that user will go to
	 */
	private ArrayList<String> toSubject;
	
	/**
	 * credit is ArrayList<String> that collect every credit
	 * that user transfer
	 */
	private ArrayList<String> credit;
	
	/**
	 * grade is ArrayList<String> that collect every grade
	 * that user transfer
	 */
	private ArrayList<String> grade;
	
	/**
	 * Constructor of Searcher that use for create Searcher
	 */
	public Searcher(){
		this.select=null;
		this.key=null;
		this.setFsub(new ArrayList<String>());
		this.setTsub(new ArrayList<String>());
		this.setGrade(new ArrayList<String>());
		this.setCredit(new ArrayList<String>());
	}
	
	/**
	 * getSelect is method that get type of search
	 * @return select is String that type of search
	 */
	public String getSelect() {
		return select;
	}
	
	/**
	 * setSelect is method that set type of search
	 * @param select is String that type of search
	 */
	public void setSelect(String select) {
		this.select = select;
	}
	
	/**
	 * getKey is method that get key of search
	 * @return key is String that the key searching
	 */
	public String getKey() {
		return key;
	}
	
	/**
	 * setKey is method that set key of search
	 * @param key is String that the key searching
	 */
	public void setKey(String key) {
		this.key = key;
	}
	
	/**
	 * getGrade is method that get all grade of key
	 * @return grade is ArrayList<String> that all grade of key
	 */
	public ArrayList<String> getGrade() {
		return grade;
	}
	
	/**
	 * setGrade is method that set all grade of key
	 * @param grade is ArrayList<String> that all grade of key
	 */
	public void setGrade(ArrayList<String> grade) {
		this.grade = grade;
	}
	
	/**
	 * getCredit is method that get all credit of key
	 * @return credit is ArrayList<String> that all credit of key
	 */
	public ArrayList<String> getCredit() {
		return credit;
	}
	
	/**
	 * setCredit is method that set all credit of key
	 * @param credit is ArrayList<String> that all credit of key
	 */
	public void setCredit(ArrayList<String> credit) {
		this.credit = credit;
	}
	
	/**
	 * getTsub is method that get all subject-id of key which 
	 * user will go to
	 * @return toSubject is ArrayList<String> that all subject-id of 
	 * key that user will go to
	 */
	public ArrayList<String> getTsub() {
		return toSubject;
	}
	
	/**
	 * setTsub is method that set all subject-id of key which
	 * user will go to
	 * @param tsub is ArrayList<Stirng> that all subject-id of key
	 * that user will go to
	 */
	public void setTsub(ArrayList<String> tsub) {
		this.toSubject = tsub;
	}
	
	/**
	 * getFsub is method that get all subject-id of key which
	 * user come from
	 * @return fromSubject is ArrayList<String> that all subject-id of key
	 * that user come from
	 */
	public ArrayList<String> getFsub() {
		return fromSubject;
	}
	
	/**
	 * setFsub is method that set all subject-id of key which 
	 * user come from 
	 * @param fsub is ArrayLsit<String> that all subject-id of key 
	 * that user come from
	 */
	public void setFsub(ArrayList<String> fsub) {
		this.fromSubject = fsub;
	}
	
	/**
	 * Search is method that use for search detail of
	 * searching type and searching key
	 * @param select is String that type of search
	 * @param key is String that key of search
	 */
	public void Search(String select,String key){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectionUrl = "jdbc:mysql://localhost:3306/DataProject";
			String connectionUser = "root";
			String connectionPassword = "";
			conn = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = conn.createStatement();
			String sql = null;
			sql = "SELECT * FROM `tb-Student` WHERE `"+select+"`=\'"+key+"\'";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String funi = rs.getString("From_University");
				String ffac = rs.getString("From_Faculty");
				String fmaj = rs.getString("From_Major");
				String tuni = rs.getString("To_University");
				String tfac = rs.getString("To_Faculty");
				String tmaj = rs.getString("To_Major");
				String fsub = rs.getString("From_SubjectID");
				String tsub = rs.getString("To_SubjectID");
				String cre = rs.getString("Credit");
				String gra = rs.getString("Grade");
				this.setFuni(funi);
				this.setFfac(ffac);
				this.setFmaj(fmaj);
				this.setTuni(tuni);
				this.setTfac(tfac);
				this.setTmaj(tmaj);
				this.fromSubject.add(fsub);
				this.toSubject.add(tsub);
				this.credit.add(cre);
				this.grade.add(gra);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	
	/**
	 * getFuni is method that get university of key which
	 * user come from
	 * @return fromUniversity is String that name of university
	 * of key which user come from
	 */
	public String getFuni() {
		return fromUniversity;
	}
	
	/**
	 * setFuni is method that set university of key which
	 * user will go to
	 * @param funi is String that name of university of key which user
	 * come from
	 */
	public void setFuni(String funi) {
		this.fromUniversity = funi;
	}
	
	/**
	 * getFfac is method that get faculty of key which
	 * user come from
	 * @return getFfac is String that name of faculty of key which
	 * user come from
	 */
	public String getFfac() {
		return fromFaculty;
	}
	
	/**
	 * setFfac is method that set faculty of key which
	 * user come from
	 * @param ffac is String that name of faculty of key which
	 * user come from
	 */
	public void setFfac(String ffac) {
		this.fromFaculty = ffac;
	}
	
	public String getFmaj() {
		return fromMajor;
	}
	public void setFmaj(String fmaj) {
		this.fromMajor = fmaj;
	}
	public String getTuni() {
		return toUniversity;
	}
	public void setTuni(String tuni) {
		this.toUniversity = tuni;
	}
	public String getTfac() {
		return toFaculty;
	}
	public void setTfac(String tfac) {
		this.toFaculty = tfac;
	}
	public String getTmaj() {
		return toMajor;
	}
	public void setTmaj(String tmaj) {
		this.toMajor = tmaj;
	}
}
