package creditTransfer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
/**
 * Adaptee
 * 
 * */
public class University{
	private String name;
	private String faculty;
	private String major;
	private ArrayList<String> keys;
	private ArrayList<String> facultys;
	private ArrayList<String> majors;
	private ArrayList<String> subjectIds;
	private ArrayList<String> subjectNames;
	private ArrayList<String> credits;

	public University(String name){
		this.setName(name);
		this.setKeys(new ArrayList<String>());
		this.setFacultys(new ArrayList<String>());
		this.setMajors(new ArrayList<String>());
		this.setSubjectIds(new ArrayList<String>());
		this.setSubjectNames(new ArrayList<String>());
		this.setCredits(new ArrayList<String>());
		this.initName(this.name);
	}

	public University(String name,String faculty,String major){
		this.setName(name);
		this.setFaculty(faculty);
		this.setMajor(major);
		this.setKeys(new ArrayList<String>());
		this.setFacultys(new ArrayList<String>());
		this.setMajors(new ArrayList<String>());
		this.setSubjectIds(new ArrayList<String>());
		this.setSubjectNames(new ArrayList<String>());
		this.setCredits(new ArrayList<String>());
		this.initAll(this.name,this.faculty,this.major);
	}

	public String[] getFacultyFromSelectedUniversity(String name){
		this.setName(name);
		this.initName(this.name);
		return getAllFaculty();
	}
	public void initName(String name){
		this.SQL("SELECT * FROM "+"`tb_"+name+"`");
	}
	public void initAll(String name,String faculty,String major){
		this.SQL("SELECT * FROM "+"`tb_"+name+"` WHERE `Faculty` = \'"+faculty+"\' AND `Major` = \'"+major+"\'");
	}

	private void SQL(String query){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();;
			String connectionUrl = "jdbc:mysql://localhost:3306/DataProject";
			String connectionUser = "root";
			String connectionPassword = "";
			conn = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				String key = rs.getString("Barcode");
				String fac = rs.getString("Faculty");
				String maj = rs.getString("Major");
				String subId = rs.getString("SubjectID");
				String subName = rs.getString("SubjectName");
				String cre = rs.getString("Credit");
				this.getKeys().add(key);
				this.getFacultys().add(fac);
				this.getMajors().add(maj);
				this.getSubjectIds().add(subId);
				this.getSubjectNames().add(subName);
				this.getCredits().add(cre);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}


	public String[] getAllFaculty() {
		ArrayList<String> fac = new ArrayList<String>();
		for(int i=0 ; i<this.getFacultys().size() ; i++){
			if(i==0)fac.add(this.getFacultys().get(i));
			if(!fac.contains(this.getFacultys().get(i))) fac.add(this.getFacultys().get(i));
		}
		String[] faculty = new String[fac.size()];
		for(int i=0 ; i<fac.size() ; i++){
			faculty[i]=fac.get(i);
		}
		return faculty;
	}

	public String[] getAllMajor(String faculty) {
		ArrayList<String> maj2 = new ArrayList<String>();
		for(int i=0 ; i<this.majors.size() ; i++){
			if(this.facultys.get(i).equalsIgnoreCase(faculty)) maj2.add(this.majors.get(i));
		}		
		ArrayList<String> maj = new ArrayList<String>();
		for(int i=0 ; i<maj2.size() ; i++){
			if(i==0)maj.add(maj2.get(i));
			if(!maj.contains(maj2.get(i))) maj.add(maj2.get(i));
		}
		String[] major = new String[maj.size()];
		for(int i=0 ; i<maj.size() ; i++){
			major[i]=maj.get(i);
		}
		return major;
	}

	public ArrayList<String> getKeys() {
		return keys;
	}

	public void setKeys(ArrayList<String> keys) {
		this.keys = keys;
	}

	public ArrayList<String> getFacultys() {
		return facultys;
	}

	public void setFacultys(ArrayList<String> facultys) {
		this.facultys = facultys;
	}

	public ArrayList<String> getMajors() {
		return majors;
	}

	public void setMajors(ArrayList<String> majors) {
		this.majors = majors;
	}

	public ArrayList<String> getSubjectIds() {
		return subjectIds;
	}

	public void setSubjectIds(ArrayList<String> subjectIds) {
		this.subjectIds = subjectIds;
	}

	public ArrayList<String> getSubjectNames() {
		return subjectNames;
	}

	public void setSubjectNames(ArrayList<String> subjectNames) {
		this.subjectNames = subjectNames;
	}

	public ArrayList<String> getCredits() {
		return credits;
	}

	public void setCredits(ArrayList<String> credits) {
		this.credits = credits;
	}

	public String getName() {
		return name;
	}

	public void setName(String name){
		this.name = name;
	}
	
	public void setFromUniversity(String name) {
		this.setName(name);
		this.keys.clear();
		this.facultys.clear();
		this.majors.clear();
		this.subjectIds.clear();
		this.subjectNames.clear();
		this.credits.clear();
		this.initName(this.name);
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}
	public String getCreditFromSubjectID(String subjectID){
		int index = this.subjectIds.indexOf(subjectID);
		return this.credits.get(index);
	}
	public String getSubjectNameFromSubjectID(String subjectID){
		int index = this.subjectIds.indexOf(subjectID);
		return this.subjectNames.get(index);
	}
}
