package creditTransfer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;

/**
 * Saver is class that use for save student-formation to the data base
 * 
 * @author nattapat
 * @author kundjanasith
 *
 */
public class Saver {

	/**
	 * student is Student that use this program for transfer credit
	 */
	private Student student;

	/**
	 * Constructor of Saver that use for create Saver
	 * @param user
	 */
	public Saver(Student user){
		this.student=user;
		this.init();
	}

	/**
	 * init is method that use for intialize value of Saver
	 */
	public void init(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectionUrl = "jdbc:mysql://localhost:3306/DataProject";
			String connectionUser = "root";
			String connectionPassword = "";
			conn = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = conn.createStatement();
			String insertStr = null;
			insertStr = "INSERT INTO `tb-Student` VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";	
			PreparedStatement prepared = (PreparedStatement) conn.prepareStatement(insertStr);
			for(int i=0 ; i<student.getToSubjectId().size() ; i++){
				prepared.setString(1, student.getFirstname());
				prepared.setString(2, student.getLastname());
				prepared.setString(3, student.getID());
				prepared.setString(4, student.getFrom().getName());
				prepared.setString(5, student.getFrom().getFaculty());
				prepared.setString(6, student.getFrom().getMajor());
				prepared.setString(7, student.getTo().getName());
				prepared.setString(8, student.getTo().getFaculty());
				prepared.setString(9, student.getTo().getMajor());
				prepared.setString(10, student.getFromSubjectId().get(i));
				prepared.setString(11, student.getFromSubjectName().get(i));
				prepared.setString(12, student.getToSubjectId().get(i));
				prepared.setString(13, student.getToSubjectName().get(i));
				prepared.setString(14, student.getCredit().get(i));
				prepared.setString(15, student.getGrade().get(i));
				prepared.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}

}
