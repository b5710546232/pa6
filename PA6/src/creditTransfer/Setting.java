package creditTransfer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

public class Setting {
	
	private static ArrayList<String> universitys ;
	
	public Setting(){ 
		setUniversitys(new ArrayList<String>());
		this.setCurrentUniversity(this.getCurrentUniversityName());
		this.initSQL();
	}

	public void initSQL(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();;
			String connectionUrl = "jdbc:mysql://localhost:3306/DataProject";
			String connectionUser = "root";
			String connectionPassword = "";
			conn = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM `tb-All.University` ");
			while (rs.next()) {
				getUniversitys().add(rs.getString("University"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}

	public ArrayList<String> getAllUniversity(){
		return getUniversitys();
	}
	
	public void CreateUni(String University) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();;
			String connectionUrl = "jdbc:mysql://localhost:3306/DataProject";
			String connectionUser = "root";
			String connectionPassword = "";
			conn = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = conn.createStatement();
			String insertStr = "CREATE TABLE `DataProject`.`tb_"+University+"` ( `Barcode` TEXT NOT NULL , `Faculty` TEXT NOT NULL , `Major` TEXT NOT NULL , `SubjectID` TEXT NOT NULL , `SubjectName` TEXT NOT NULL , `Credit` TEXT NOT NULL) ENGINE = InnoDB";
			PreparedStatement prepared = (PreparedStatement) conn.prepareStatement(insertStr);
			prepared.executeUpdate();
			String insertStr1 = "INSERT INTO `tb-All.University`(`University`) VALUES (?)";
			PreparedStatement prepared1 = (PreparedStatement) conn.prepareStatement(insertStr1);
			prepared1.setString(1,University);
			prepared1.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
	public String getCurrentUniversityName(){
		String x = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();;
			String connectionUrl = "jdbc:mysql://localhost:3306/DataProject";
			String connectionUser = "root";
			String connectionPassword = "";
			conn = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM `tb-Set.University`");
			while (rs.next()) {
				String setUni = rs.getString("University");
				x=setUni;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		return x;
	}
	
	public University getCurrentUnversity(){
		return new University(this.getCurrentUniversityName());
	}
	
	public void setCurrentUniversity(String name){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();;
			String connectionUrl = "jdbc:mysql://localhost:3306/DataProject";
			String connectionUser = "root";
			String connectionPassword = "";
			conn = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = conn.createStatement();
			String insertStr1 = "INSERT INTO `tb-Set.University`(`University`) VALUES (?)";
			PreparedStatement prepared1 = (PreparedStatement) conn.prepareStatement(insertStr1);
			prepared1.setString(1,name);
			prepared1.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}

	public static ArrayList<String> getUniversitys() {
		return universitys;
	}

	public static void setUniversitys(ArrayList<String> universitys) {
		Setting.universitys = universitys;
	}
	
}
