package creditTransfer;
/**
 * Adpater
 * @author nattapat
 * @author kundjanasith
 */
public class TransferCredit implements Transfer{
	
	private University from;
	private University to;

	public TransferCredit(University from,University to){
		this.from=from;
		this.to=to;
	}

	@Override
	public String getTransSubjectId(String subjectId) {
		if(this.from.getSubjectIds().contains(subjectId)){
			int index = this.from.getSubjectIds().indexOf(subjectId);
			if(this.to.getKeys().contains(this.from.getKeys().get(index))){
				int muIndex = this.to.getKeys().indexOf(this.from.getKeys().get(index));
				return this.to.getSubjectIds().get(muIndex);
			}
		}
		return null;
	}
}
