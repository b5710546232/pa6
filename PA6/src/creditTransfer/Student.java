package creditTransfer;

import java.util.ArrayList;

public class Student {
	private String firstname;
	private String lastname;
	private String ID;
	private University from;
	private University to;
	private ArrayList<String> fromSubjectId;
	private ArrayList<String> fromSubjectName;
	private ArrayList<String> toSubjectId;
	private ArrayList<String> toSubjectName;
	private ArrayList<String> credit;
	private ArrayList<String> grade;
	public Student(String[] inform,University to,University from){
		this.setFirstname(inform[0]);
		this.setLastname(inform[1]);
		this.setID(inform[2]);
		this.setTo(to);
		this.setFrom(from);
		this.setFromSubjectId(new ArrayList<String>());
		this.setFromSubjectName(new ArrayList<String>());
		this.setToSubjectId(new ArrayList<String>());
		this.setToSubjectName(new ArrayList<String>());
		this.setCredit(new ArrayList<String>());
		this.setGrade(new ArrayList<String>());
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public University getFrom() {
		return from;
	}
	public void setFrom(University from) {
		this.from = from;
	}
	public University getTo() {
		return to;
	}
	public void setTo(University to) {
		this.to = to;
	}
	public ArrayList<String> getFromSubjectId() {
		return fromSubjectId;
	}
	public void setFromSubjectId(ArrayList<String> fromSubjectId) {
		this.fromSubjectId = fromSubjectId;
	}
	public ArrayList<String> getFromSubjectName() {
		return fromSubjectName;
	}
	public void setFromSubjectName(ArrayList<String> fromSubjectName) {
		this.fromSubjectName = fromSubjectName;
	}
	public ArrayList<String> getToSubjectId() {
		return toSubjectId;
	}
	public void setToSubjectId(ArrayList<String> toSubjectId) {
		this.toSubjectId = toSubjectId;
	}
	public ArrayList<String> getToSubjectName() {
		return toSubjectName;
	}
	public void setToSubjectName(ArrayList<String> toSubjectName) {
		this.toSubjectName = toSubjectName;
	}
	public ArrayList<String> getCredit() {
		return credit;
	}
	public void setCredit(ArrayList<String> credit) {
		this.credit = credit;
	}
	public void addFormSubjectId(String subjectId){
		this.fromSubjectId.add(subjectId);
	}
	public void addFormSubjectName(String subjectName){
		this.fromSubjectName.add(subjectName);
	}
	public void addToSubjectId(String subejctId){
		this.toSubjectId.add(subejctId);
	}
	public void addToSubjectName(String subjectName){
		this.toSubjectName.add(subjectName);
	}
	public void addCredit(String credit){
		this.credit.add(credit);
	}
	public int getTotalSubject(){
		return this.credit.size();
	}
	public int getTotalCredit(){
		int credit = 0;
		for(int i=0 ; i<this.credit.size() ; i++){
			credit+=Integer.parseInt(this.credit.get(i));
		}
		return credit;
	}
	public void addGrade(String grade){
		this.grade.add(grade);
	}
	public ArrayList<String> getGrade() {
		return grade;
	}
	public void setGrade(ArrayList<String> grade) {
		this.grade = grade;
	}
}
