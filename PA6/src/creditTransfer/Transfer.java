package creditTransfer;
/**
 * target - in Adapter Pattern.
 * */

public interface Transfer {
	
	String getTransSubjectId(String subjectId);
	
}
