import javax.swing.SwingUtilities;

import UI.MainUI;


public class App {

	public static void main(String[] args) {
		MainUI main = new  MainUI();
		SwingUtilities.invokeLater(main);
	}

}
