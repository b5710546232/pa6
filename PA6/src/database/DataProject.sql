-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 06, 2015 at 01:59 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `DataProject`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb-All.University`
--

CREATE TABLE IF NOT EXISTS `tb-All.University` (
  `University` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb-All.University`
--

INSERT INTO `tb-All.University` (`University`) VALUES
('K.University'),
('C.University'),
('M.University'),
('T.University');

-- --------------------------------------------------------

--
-- Table structure for table `tb-Set.University`
--

CREATE TABLE IF NOT EXISTS `tb-Set.University` (
  `University` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb-Set.University`
--

INSERT INTO `tb-Set.University` (`University`) VALUES
('K.University');

-- --------------------------------------------------------

--
-- Table structure for table `tb-Student`
--

CREATE TABLE IF NOT EXISTS `tb-Student` (
  `Firstname` text NOT NULL,
  `Lastname` text NOT NULL,
  `ID` text NOT NULL,
  `From_University` text NOT NULL,
  `From_Faculty` text NOT NULL,
  `From_Major` text NOT NULL,
  `To_University` text NOT NULL,
  `To_Faculty` text NOT NULL,
  `To_Major` text NOT NULL,
  `From_SubjectId` text NOT NULL,
  `From_SubjectName` text NOT NULL,
  `To_SubjectId` text NOT NULL,
  `To_SubjectName` text NOT NULL,
  `Credit` text NOT NULL,
  `Grade` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_C.University`
--

CREATE TABLE IF NOT EXISTS `tb_C.University` (
  `Barcode` text NOT NULL,
  `Faculty` text NOT NULL,
  `Major` text NOT NULL,
  `SubjectID` text NOT NULL,
  `SubjectName` text NOT NULL,
  `Credit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_C.University`
--

INSERT INTO `tb_C.University` (`Barcode`, `Faculty`, `Major`, `SubjectID`, `SubjectName`, `Credit`) VALUES
('MAT01TY01', 'Engineering', 'Information and Communication', '02301107', 'Calculus 1', '3'),
('PHY01TY02', 'Engineering', 'Information and Communication', '02304153', 'Physics for Engineers', '3'),
('MAT03TY02', 'Engineering', 'Information and Communication', '02190200', 'Discrete Structure', '3'),
('DAT01TY01', 'Engineering', 'Information and Communication', '02190422', 'Database Systems', '3'),
('IIT02TY01', 'Engineering', 'Information and Communication', '02190213', 'Principles of Information Systems', '3'),
('CHE01TY02', 'Engineering', 'Nano', '02302105', 'Chemistry for Engineers', '3'),
('EEW01TY01', 'Engineering', 'Nano', '02140111', 'Exploring Engineering World', '3'),
('MAT01TY01', 'Engineering', 'Nano', '02301107', 'Calculus 1', '3'),
('PHY01TY02', 'Engineering', 'Nano', '02304153', 'Physics for Engineers', '3'),
('PHY05TY01', 'Engineering', 'Nano', '02304154', 'Physics and Electronics for Engineers', '3'),
('MAT02TY02', 'Science', 'Mathematics', '02301117', 'Calculus 2', '3'),
('MAT02TY01', 'Engineering', 'Information and Communication', '02301108', 'Calculus 2', '3'),
('MAT02TY01', 'Engineering', 'Nano', '02301108', 'Calculus 2', '3'),
('COM05TY01', 'Engineering', 'Information and Communication', '02190250', '	Computer Architecture and Organization', '3');

-- --------------------------------------------------------

--
-- Table structure for table `tb_K.University`
--

CREATE TABLE IF NOT EXISTS `tb_K.University` (
  `Barcode` text NOT NULL,
  `Faculty` text NOT NULL,
  `Major` text NOT NULL,
  `SubjectID` text NOT NULL,
  `SubjectName` text NOT NULL,
  `Credit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_K.University`
--

INSERT INTO `tb_K.University` (`Barcode`, `Faculty`, `Major`, `SubjectID`, `SubjectName`, `Credit`) VALUES
('THA01TY01', 'Engineering', 'Software and Knowledge', '01999021', 'Thai Language for Communication', '3'),
('THA01TY01', 'Engineering', 'Environmental', '01999021', 'Thai Language for Communication', '3'),
('PHY01TY01', 'Engineering', 'Software and Knowledge', '01420111', 'General Physics 1', '3'),
('PHY01TY01', 'Engineering', 'Environmental', '01420111', 'General Physics 1', '3'),
('MAT01TY01', 'Engineering', 'Software and Knowledge', '01417167', 'Engineering Mathematics 1', '3'),
('MAT01TY01', 'Engineering', 'Environmental', '01417167', 'Engineering Mathematics 1', '3'),
('CHE01TY01', 'Engineering', 'Environmental', '01403117', 'Fundamental of General Chemistry', '3'),
('DAT01TY01', 'Engineering', 'Software and Knowledge', '01204351', 'Database Systems', '3'),
('MAT03TY01', 'Engineering', 'Software and Knowledge', '01204211', 'Discrete Mathematics', '3'),
('CHE05TY01', 'Engineering', 'Environmental', '01203211', 'Surveying', '3'),
('IIT02TY01', 'Engineering', 'Software and Knowledge', '01516187', 'Systems of Information', '3'),
('MAT02TY01', 'Engineering', 'Software and Knowledge', '01417168', 'Engineering Mathematics 2', '3'),
('MAT02TY01', 'Engineering', 'Environmental', '01417168', 'Engineering Mathematics 2', '3'),
('COM05TY01', 'Engineering', 'Software and Knowledge', '01219221', ' Computer Organization and Architecture', '3');

-- --------------------------------------------------------

--
-- Table structure for table `tb_M.University`
--

CREATE TABLE IF NOT EXISTS `tb_M.University` (
  `Barcode` text NOT NULL,
  `Faculty` text NOT NULL,
  `Major` text NOT NULL,
  `SubjectID` text NOT NULL,
  `SubjectName` text NOT NULL,
  `Credit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_M.University`
--

INSERT INTO `tb_M.University` (`Barcode`, `Faculty`, `Major`, `SubjectID`, `SubjectName`, `Credit`) VALUES
('MAT03TY01', 'Science', 'Computer', 'ICMA242', 'Discrete Mathematics', '3'),
('MAT01TY01', 'Science', 'Computer', 'ICMA211', 'General Mathematics 1', '3'),
('PHY01TY01', 'Science', 'Computer', 'ICPY211', 'General Physics 1', '3'),
('DAT01TY02', 'Science', 'Computer', 'ICCS223', 'Data Communications and Networks', '3'),
('DAT05TY01', 'Science', 'Computer', 'ICCS307', 'Design Patterns', '3'),
('EEW01TY01', 'Science', 'Food Science and Technology', 'ICCM202', 'Exploring Global Realities', '3'),
('MAT02TY01', 'Science', 'Food Science and Technology', 'ICNS015', 'Refresher Mathematics', '3'),
('THA02TY01', 'Science', 'Food Science and Technology', 'ICEG232', 'Advanced Oral Communication', '3'),
('EEW02TY01', 'Science', 'Food Science and Technology', 'ICNS256', 'Sustainable Development', '3'),
('EEW03TY02', 'Science', 'Food Science and Technology', 'ICEG250', 'Introduction to Linguistics', '3');

-- --------------------------------------------------------

--
-- Table structure for table `tb_T.University`
--

CREATE TABLE IF NOT EXISTS `tb_T.University` (
  `Barcode` text NOT NULL,
  `Faculty` text NOT NULL,
  `Major` text NOT NULL,
  `SubjectID` text NOT NULL,
  `SubjectName` text NOT NULL,
  `Credit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_T.University`
--

INSERT INTO `tb_T.University` (`Barcode`, `Faculty`, `Major`, `SubjectID`, `SubjectName`, `Credit`) VALUES
('MAT01TY03', 'Engineering', 'Computer', 'GTS111', 'Mathematical Analysis for Management Science', '3'),
('PHY01TY02', 'Engineering', 'Computer', 'SCS138', 'Applied Physics 1', '3'),
('DAT01TY02', 'Engineering', 'Computer', 'CSS331', 'Fundamental of Data Communication', '3'),
('THA01TY01', 'Engineering', 'Computer', 'ECS332', 'Principles of Communication', '3'),
('MAT03TY01', 'Engineering', 'Computer', 'ITS201', 'Discrete Mathematics', '3'),
('MAT01TY03', 'Engineering', 'Chemical', 'GTS111', 'Mathematical Analysis for Management Science', '3'),
('SCI01TY01', 'Engineering', 'Chemical', 'GTS121', 'General Science 1', '3'),
('SCI05TY01', 'Engineering', 'Chemical', 'GTS133', 'Environmental Studies', '3'),
('EEW02TY01', 'Engineering', 'Chemical', 'IES331', 'Quality Control', '3'),
('MAT02TY01', 'Engineering', 'Chemical', 'MAS116', 'Mathematics1', '3');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
