package UI;

import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Font;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JScrollPane;

import creditTransfer.Searcher;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * searchUI is UI for searching.
 * */
public class searchUI implements Runnable{
	private JLabel bg;
	private JLabel searchTypeLabel;
	private JComboBox searchTypeComboBox;
	private static JFrame frame;
	private Container contents;
	private JTextField searchText;
	private JButton searchButton;
	private JButton backButton;
	private JLabel from;
	private JLabel to; 
	private String [][] dataRow = new String[50][4];
	private static String[] Header1 = {"From-SubjectId","To-SubjectId","Credit","Grade"};
	private static String[] searchType = {"search from Firstname","search from Lastname","search from ID"};
	private static JTable table;
	private static JScrollPane scrollPane;
	private static URL url_bg;
	private static ImageIcon bgImageIcon; 
	Class loader = this.getClass();
	/**
	 * Constructor of  SearchUI.
	 * */
	public searchUI (){
		frame = new JFrame("Credit Transfer");
		frame.setBounds(30, 30, 800, 600);
		initComponent();

	}
	/**
	 * get Frame of this UI.
	 * @return frame of UI.
	 * */
	public JFrame getFrame(){
		return frame;
	}
	/**
	 * init all Component UI.
	 * */
	private void initComponent(){
		contents = frame.getContentPane();
		this.bg = new JLabel();
		this.bg.setBounds(0, 0, 800, 578);
		initText();
		initComboBox();
		initButton();
		initTable();
		initLabel();
		initImages();
		contents.add(bg);
	}
	/**
	 * init the Table.
	 * */
	private void initTable(){
		scrollPane = new JScrollPane();
		scrollPane.setBounds(46, 328, 529, 227);
		contents.add(scrollPane);
		table = new JTable(dataRow,Header1);
		table.setGridColor(Color.black);
		scrollPane.setViewportView(table);
	}
	/**
	 * init all Button and ActionListener.
	 * */
	private void initButton(){
		searchButton = new JButton("");
		searchButton.setIcon(new ImageIcon(loader.getResource("/images/searchButtonclick.png")));
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String select = (searchTypeComboBox.getSelectedItem()+"").split(" ")[2];
				String key = searchText.getText();
				clearText();
				Searcher search = new Searcher();
				search.Search(select, key);
				if(search.getFuni()==null){
					JOptionPane.showMessageDialog(frame,"Can not found . . .","Warning!",JOptionPane.ERROR_MESSAGE);
				}
				else{
					from.setText("From: University: "+search.getFuni()+"  Faculty: "+search.getFfac()+" Major: "+search.getFmaj());
					to.setText("To: University: "+search.getTuni()+"  Faculty: "+search.getTfac()+" Major: "+search.getTmaj());
					setTextTable(search);
				}
			}

		});
		searchButton.setBounds(473, 177, 100, 50);
		contents.add(searchButton);

		backButton = new JButton("");
		backButton.setIcon(new ImageIcon(loader.getResource("/images/BackToMenu.png")));
		backButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				MainUI.getFrame().setLocation(frame.getLocation());
				MainUI.getFrame().setVisible(true);
				frame.setVisible(false);

			}

		});
		backButton.setBounds(652, 487, 125, 50);
		contents.add(backButton);
	}
	/**
	 * init all images of this UI.
	 * */
	private void initImages(){
		url_bg = loader.getResource("/images/SearchBG.png");
		bgImageIcon  =  new ImageIcon(url_bg);
		this.bg.setIcon(bgImageIcon);
	}
	/**
	 * init all JTextField of this UI.
	 * */

	private void initText(){
		searchText = new JTextField(20);
		searchText.setBounds(44, 182, 371, 44);
		contents.add(searchText);
	}
	
	/**
	 * init all JLabel of this UI.
	 * */
	private void initLabel(){
		frame.getContentPane().setLayout(null);
		searchTypeLabel = new JLabel("Select what you want to Search : ");
		searchTypeLabel.setBounds(46, 130, 469, 35);
		searchTypeLabel.setFont(new Font("Helvetica", Font.PLAIN, 25));
		searchTypeLabel.setForeground(Color.WHITE);
		contents.add(searchTypeLabel);		
		from = new JLabel();
		from.setBounds(54, 250, 736, 16);
		from.setForeground(Color.white);
		frame.getContentPane().add(from);
		to = new JLabel();
		to.setBounds(54, 288, 726, 16);
		to.setForeground(Color.white);
		frame.getContentPane().add(to);
	}
	/**
	 * init all JComboBox of this UI.
	 * */
	private void initComboBox(){
		searchTypeComboBox = new JComboBox(searchType );
		searchTypeComboBox.setFont(new Font("Helvetica", Font.PLAIN, 15));
		searchTypeComboBox.setSize(241, 34);
		searchTypeComboBox.setLocation(440, 131);
		contents.add(searchTypeComboBox);
	}
	/**
	 * setting the Text of Table.
	 * */
	private void setTextTable(Searcher search) {
		for(int i=0 ; i<search.getFsub().size() ; i++){
			table.setValueAt(search.getFsub().get(i),i,0);
			table.setValueAt(search.getTsub().get(i),i,1);
			table.setValueAt(search.getCredit().get(i),i,2);
			table.setValueAt(search.getGrade().get(i),i,3);
		}
	}
	/**
	 * run for running this UI.
	 * */
	public void run() {
		frame.setVisible(true);
		this.frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
	}
	/**
	 * clear all JtextField.
	 * */
	private void clearText(){
		from.setText("");
		to.setText("");
		for(int i=0 ; i<dataRow.length ; i++){
			table.setValueAt("",i,0);
			table.setValueAt("",i,1);
			table.setValueAt("",i,2);
			table.setValueAt("",i,3);
		}
	}

}
