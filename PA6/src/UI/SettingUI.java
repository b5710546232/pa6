package UI;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.net.URL;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import creditTransfer.*;

import java.awt.FlowLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPasswordField;
/**
 * SettingUI is UI of setting.
 * @author nattapat
 * @author kundjanasith
 * */
public class SettingUI implements Runnable{
	private JLabel bg;
	private JLabel addLabel;
	private JLabel selectOwnLabel;
	private JTextField addUniversityText;
	private JFrame frame;
	private JComboBox universityBox;
	Container contents ;
	private JLabel currentUniversity;
	private static Setting setting;
	private Point location;
	private JButton addButton;
	private JButton selectButton;
	private JButton backButton;
	private DefaultComboBoxModel modelSelected;
	private static ImageIcon bgImageIcon;
	private static URL url_bg;
	private JPasswordField passwordField;
	private JLabel admin;
	private JButton Confirm;
	private Class loader = this.getClass();
	/**
	 * Constructor of SettingUI
	 * @param setting for set the value in setting.
	 * */
	public SettingUI (Setting setting){
		this.setting = setting;
		frame = new JFrame("Credit Transfer");
		contents = frame.getContentPane();
		contents.setLayout(null);
		frame.setBounds(0, 0, 800, 600);
		initLabel();
		initText();
		initBox();
		initButton();
		initComponent();
	}
	/**
	 * get the frame.
	 * @return frame of this UI.
	 * */
	public  JFrame getFrame(){
		return frame;
	}

	public Container getContents(){
		return frame.getContentPane();
	}

	/**
	 * init the all Component of UI.
	 * */
	private void initComponent(){
		this.bg = new JLabel();
		bg.setBounds(0, 0, 800, 578);


		this.initImages();
		contents.add(bg);
		
	}
	/**
	 * init the all JComboBox of UI.
	 * */
	private void initBox(){
		JPanel panel = new JPanel();
		panel.setSize(211, 63);
		panel.setLocation(150, 404);
		modelSelected = new DefaultComboBoxModel(setting.getAllUniversity().toArray());
		universityBox = new JComboBox();
		universityBox.setEnabled(false);
		universityBox.setModel(modelSelected);
		universityBox.setFont(new Font("Helvetica", Font.PLAIN, 15));
		universityBox.setBounds(150, 392, 328, 44);
		universityBox.setSize(328, 50);
		contents.add(universityBox);

	}
	/**
	 * init the all JLabel of UI.
	 * */
	private void initLabel(){
		addLabel = new JLabel("Add new University to Data Base");
		addLabel.setFont(new Font("Helvetica", Font.PLAIN, 30));
		addLabel.setForeground(Color.WHITE);;
		addLabel.setBounds(33, 208, 539, 30);
		contents.add(addLabel);
		selectOwnLabel = new JLabel("Select your University");
		selectOwnLabel.setFont(new Font("Helvetica", Font.PLAIN, 30));
		selectOwnLabel.setForeground(Color.WHITE);
		selectOwnLabel.setBounds(33, 349, 315, 30);
		contents.add(selectOwnLabel);
		currentUniversity  = new JLabel(" Credits Tranfer of : "+setting.getCurrentUnversity().getName());
		currentUniversity.setFont(new Font("Helvetica", Font.PLAIN, 20));
		currentUniversity.setForeground(Color.WHITE);
		currentUniversity.setBounds(33, 523, 592, 30);
		contents.add(currentUniversity);
		admin = new JLabel("Admin:");
		admin.setForeground(Color.WHITE);
		admin.setFont(new Font("Helvetica", Font.PLAIN, 25));
		admin.setBounds(33, 127, 115, 36);
		frame.getContentPane().add(admin);
		passwordField = new JPasswordField();
		passwordField.setBounds(160, 131, 318, 33);
		frame.getContentPane().add(passwordField);
	}
	/**
	 * init the all JButton of UI.
	 * each Button has ActionListener.
	 * */
	private void initButton(){
		
		Confirm = new JButton("");
		Confirm.setIcon(new ImageIcon(loader.getResource("/images/confirmButton.png")));
		Confirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(passwordField.getText().equalsIgnoreCase("123")){
					addButton.setEnabled(true);
					selectButton.setEnabled(true);
					addUniversityText.setEnabled(true);
					universityBox.setEnabled(true);
				}
				else {
					JOptionPane.showMessageDialog(frame,"Incorrect Password...","Warning !",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		Confirm.setBounds(508, 134, 100, 25);
		frame.getContentPane().add(Confirm);

		addButton = new JButton("");
		addButton.setEnabled(false);
		addButton.setFont(new Font("Helvetica", Font.PLAIN, 15));
		addButton.setBounds(510, 274, 80, 40);
		addButton.setIcon(new ImageIcon(loader.getResource("/images/addButtonclick.png")));
		addButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				String add = addUniversityText.getText();
				if((!add.equals(""))&&!(setting.getAllUniversity().contains((String)add))) {
					System.out.println(setting.getAllUniversity());
					System.out.println("add");
					setting.CreateUni(add);
					modelSelected.addElement(add);
					setting.getAllUniversity().add(add);
					universityBox.setModel(modelSelected);
				}
				else {
					if(add.equals(""))JOptionPane.showMessageDialog(frame,"Please input your university name ","Warning!!",JOptionPane.WARNING_MESSAGE);
					else JOptionPane.showMessageDialog(frame,"Your university added to database already . . .","Warning!!",JOptionPane.WARNING_MESSAGE);
				}
			}

		});
		contents.add(addButton);

		selectButton = new JButton("");
		selectButton.setIcon(new ImageIcon(loader.getResource("/images/selectButton.png")));
		selectButton.setEnabled(false);
		selectButton.setFont(new Font("Helvetica", Font.PLAIN, 15));
		selectButton.setBounds(510, 397, 80, 40);
		selectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setting.setCurrentUniversity(universityBox.getSelectedItem()+"");
				currentUniversity.setText(" Credits Tranfer of : "+setting.getCurrentUniversityName());
				MainUI.myUniversityLabel.setText(setting.getCurrentUniversityName());
				MainUI.to =  setting.getCurrentUnversity();

			}
		});
		contents.add(selectButton);

		backButton = new JButton("");
		backButton.setIcon(new ImageIcon(loader.getResource("/images/BackToMenu.png")));
		backButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				MainUI.getFrame().setLocation(frame.getLocation());
				frame.setVisible(false);
				MainUI.getFrame().setVisible(true);
				//reset;

			}

		});
		backButton.setFont(new Font("Helvetica", Font.PLAIN, 15));
		backButton.setBounds(637, 504, 125, 50);
		contents.add(backButton);
	}

	/**
	 * init the all JTextField of UI.
	 * */
	private void initText(){
		addUniversityText = new JTextField(40);
		addUniversityText.setEnabled(false);
		addUniversityText.setFont(new Font("Helvetica", Font.PLAIN, 20));
		addUniversityText.setBounds(150, 278, 328, 36);
		contents.add(addUniversityText);
	}
	/**
	 * init the all images of UI.
	 * */
	private void initImages(){
		url_bg = loader.getResource("/images/SettingBG.png");
		bgImageIcon  =  new ImageIcon(url_bg);
		this.bg.setIcon(bgImageIcon);

	}
	/**
	 * run is for running UI.
	 * */
	public void run() {
		try {
			frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
