package UI;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.mysql.jdbc.PreparedStatement;

import creditTransfer.Setting;

import javax.swing.JPasswordField;
/**
 * AddSubjectUI of adding subject.
 * @author nattapat
 * @author kundjanasinth
 * */
public class AddSubjectUI implements Runnable{
	private static JFrame frame;
	private JLabel bg;
	private JLabel fromUniversityLabel;
	private JLabel facultyLabel;
	private JLabel majorLabel;
	private JLabel subjectLabel;
	private JLabel subjectIDLabel;
	private JLabel barCodeLabel;
	private JLabel creditLabel;
	private JLabel result;
	private JLabel result2;
	private JComboBox universityComboBox;
	private Container contents;
	private JTextField facultyText;
	private JTextField majorText;
	private JTextField subjectText;
	private JTextField subjectIDText;
	private JTextField creditText;
	private Setting setting;
	private JButton addButton;
	private JButton backButton;
	private JButton confirmButton;
	private JButton selectedButton;
	private ReportUI report;
	private JCheckBox YES;
	private JCheckBox NO;
	private static String Barcode = null;
	private static ImageIcon bgImageIcon;
	private static URL url_bg;
	private JPasswordField passwordField;
	private JButton conPass;
	private Class loader;
	/**
	 * Constuctor of AddSubjectUI
	 * @param setting for get value of this UI.
	 * */
	public AddSubjectUI (Setting setting){
		loader = this.getClass();
		frame = new JFrame("Credit Transfer");
		frame.setBounds(0, 0, 800, 600);
		contents = frame.getContentPane();
		this.setting = setting;
		this.report=new ReportUI(this.setting);
		initComponent();
	}
	/**
	 * initComponet of this UI.
	 * */
	private void initComponent(){
		this.bg = new JLabel();
		bg.setBounds(0, 0, 800, 578);
		contents.setLayout(null);
		initLabel();
		initText();
		initButton();
		initComboBox();
		initImages();
		contents.add(bg);



	}
	/**
	 * get the JFame
	 * @return frame of this UI.
	 * */
	public JFrame getFrame(){
		return this.frame;
	}

	public Container getContents(){
		return frame.getContentPane();
	}


	/**
	 * init all of JTextField
	 */
	private void initText() {
		facultyText = new JTextField(20);
		facultyText.setEnabled(false);
		facultyText.setBounds(202,341,176,30);
		contents.add(facultyText);

		majorText = new JTextField(20);
		majorText.setEnabled(false);
		majorText.setBounds(606,343,176,30);
		contents.add(majorText);

		subjectText = new JTextField(20);
		subjectText.setEnabled(false);
		subjectText.setBounds(606,385,176,30);
		contents.add(subjectText);

		subjectIDText = new JTextField(20);
		subjectIDText.setEnabled(false);
		subjectIDText.setBounds(202,385,176,30);
		contents.add(subjectIDText);

		creditText = new JTextField(20);
		creditText.setEnabled(false);
		creditText.setBounds(202,427,176,30);
		contents.add(creditText);


	}
	/**
	 * init all of JLabel.
	 * */
	private void initLabel(){				
		fromUniversityLabel = new JLabel("University :");
		fromUniversityLabel.setForeground(Color.WHITE);
		fromUniversityLabel.setFont(new Font("Helvetica", Font.PLAIN, 25));
		fromUniversityLabel.setBounds(32, 153, 159, 50);
		contents.add(fromUniversityLabel);


		facultyLabel = new JLabel("Faculty :");
		facultyLabel.setForeground(Color.WHITE);
		facultyLabel.setFont(new Font("Helvetica", Font.PLAIN, 25));
		facultyLabel.setBounds(31, 334, 125, 40);
		contents.add(facultyLabel);

		majorLabel = new JLabel("Major :"); ;
		majorLabel.setForeground(Color.WHITE);
		majorLabel.setFont(new Font("Helvetica", Font.PLAIN, 25));
		majorLabel.setBounds(418, 341, 109, 30);
		contents.add(majorLabel);

		subjectLabel = new JLabel ("Subject  Name:");
		subjectLabel.setForeground(Color.white);
		subjectLabel.setFont(new Font("Helvetica", Font.PLAIN, 25));
		subjectLabel.setBounds(418, 383, 190, 30);
		contents.add(subjectLabel);

		subjectIDLabel = new JLabel ("Subject ID :");
		subjectIDLabel.setForeground(Color.white);
		subjectIDLabel.setFont(new Font("Helvetica", Font.PLAIN, 25));
		subjectIDLabel.setBounds(31, 383, 142, 30);
		contents.add(subjectIDLabel);

		barCodeLabel = new JLabel ("Your subject can transfer with subject in my data base");
		barCodeLabel.setForeground(Color.white);
		barCodeLabel.setFont(new Font("Helvetica", Font.PLAIN, 21));
		barCodeLabel.setBounds(32, 215, 539, 30);
		contents.add(barCodeLabel);

		creditLabel = new JLabel("Credit : ");
		creditLabel.setForeground(Color.white);
		creditLabel.setFont(new Font("Helvetica", Font.PLAIN, 25));
		creditLabel.setBounds(31, 425, 142, 30);
		contents.add(creditLabel);


		result = new JLabel("Therefore");
		result.setForeground(Color.white);
		result.setFont(new Font("Helvetica", Font.PLAIN, 25));
		result.setBounds(31, 260, 763, 30);
		contents.add(result);
		result2 = new JLabel();
		result2.setForeground(Color.white);
		result2.setFont(new Font("Helvetica", Font.PLAIN, 20));
		result2.setBounds(32, 292, 762, 30);
		contents.add(result2);


		JLabel admin = new JLabel("Admin:");
		admin.setForeground(Color.WHITE);
		admin.setFont(new Font("Helvetica", Font.PLAIN, 25));
		admin.setBounds(78, 118, 113, 30);
		frame.getContentPane().add(admin);
	}
	/**
	 * init the images that use in this UI.
	 * */
	private void initImages(){
		url_bg = loader.getResource("/images/AddSubjectBG.png");
		bgImageIcon  =  new ImageIcon(url_bg);
		this.bg.setIcon(bgImageIcon);

	}
	/**
	 * init JComboBox
	 * */
	private void initComboBox(){
		universityComboBox = new JComboBox(setting.getAllUniversity().toArray());
		universityComboBox.setEnabled(false);
		universityComboBox.setBounds(203, 158, 312, 45);
		contents.add(universityComboBox);


		passwordField = new JPasswordField();
		passwordField.setBounds(203, 121, 301, 27);
		frame.getContentPane().add(passwordField);

	}
	/**
	 * initButton 
	 * each Button has actionListener.
	 * */
	private void initButton(){

		conPass = new JButton("Confirm");
		conPass.setIcon(new ImageIcon(loader.getResource("/images/confirmButton.png")));
		conPass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(passwordField.getText().equalsIgnoreCase("123")){
					passwordField.setEnabled(false);
					conPass.setEnabled(false);
					universityComboBox.setEnabled(true);
					selectedButton.setEnabled(true);
					YES.setEnabled(true);
					NO.setEnabled(true);
					confirmButton.setEnabled(true);
					addButton.setEnabled(true);
					facultyText.setEnabled(true);
					majorText.setEnabled(true);
					subjectText.setEnabled(true);
					subjectIDText.setEnabled(true);
					creditText.setEnabled(true);
				}
				else{
					JOptionPane.showMessageDialog(frame,"Incorrect Password. . .","Warning ! !",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		conPass.setBounds(544, 122, 90, 29);
		frame.getContentPane().add(conPass);

		addButton = new JButton();
		addButton.setIcon(new ImageIcon(loader.getResource("/images/addButtonclick.png")));
		addButton.setEnabled(false);
		addButton.setBounds(31,486,80,40);
		addButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean isCheckBox = !confirmButton.isEnabled()&&!selectedButton.isEnabled();
				boolean checkText = !facultyText.getText().equals("")&&!majorText.getText().equals("")&&!subjectText.getText().equals("")&&
						!subjectIDText.getText().equals("")&&!creditText.getText().equals("");
				if(checkText&&isCheckBox){
					insertToSQL();
				}
				else{
					JOptionPane.showMessageDialog(frame,"Please fill the Text","Alert",JOptionPane.INFORMATION_MESSAGE);
				}

			}

		});

		contents.add(addButton);

		backButton = new JButton();
		backButton.setIcon(new ImageIcon(loader.getResource("/images/BackToMenu.png")));
		backButton.setBounds(586,499,125,50);
		backButton.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				MainUI.getFrame().setLocation(frame.getLocation());
				MainUI.getFrame().setVisible(true);
				frame.setVisible(false);
			}

		});
		contents.add(backButton);

		confirmButton = new JButton();
		confirmButton.setIcon(new ImageIcon(loader.getResource("/images/confirmButton.png")));
		confirmButton.setEnabled(false);
		confirmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if((YES.isSelected()||NO.isSelected())&&(report.getBarCode()!=null)){
					if(NO.isSelected()) {
						String text = "Your subject can not transfer with any subject in my data base";
						result.setText(String.format("%s",text));
					}
					else{
						String trans = "Your subject can transfer with SubjectId: "+report.getSubjectId();
						result2.setText("SubjectName: "+report.getSubjectName()+" in "+report.getUniversity());
						result.setText(String.format("%s",trans));
						Barcode = report.getBarCode();
						creditText.setText(report.getCredit());
						creditText.setEditable(false);
					}
					confirmButton.setEnabled(false);
					selectedButton.setEnabled(false);
					YES.setEnabled(false);
					NO.setEnabled(false);
				}
				else{
					JOptionPane.showMessageDialog(frame,"Selected please . . .","Alert",JOptionPane.INFORMATION_MESSAGE);
				}

			}
		});
		confirmButton.setBounds(694, 215, 100, 25);
		frame.getContentPane().add(confirmButton);

		YES = new JCheckBox("YES");
		YES.setEnabled(false);
		YES.setFont(new Font("Helvetica", Font.PLAIN, 20));
		YES.setForeground(Color.WHITE);
		YES.setBounds(562, 218, 77, 23);
		frame.getContentPane().add(YES);
		YES.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(YES.isSelected())NO.setEnabled(false);
				else NO.setEnabled(true);

			}

		});


		NO = new JCheckBox("NO");
		NO.setEnabled(false);
		NO.setFont(new Font("Helvetica", Font.PLAIN, 20));
		NO.setForeground(Color.WHITE);
		NO.setBounds(633, 218, 62, 23);
		frame.getContentPane().add(NO);
		NO.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {

				if(NO.isSelected())YES.setEnabled(false);
				else YES.setEnabled(true);

			}

		});

		selectedButton = new JButton();
		selectedButton.setIcon(new ImageIcon(loader.getResource("/images/selectFromData.png")));
		selectedButton.setEnabled(false);
		selectedButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(report);
				System.out.println(report.getBarCode());
			}
		});
		selectedButton.setBounds(544, 167, 216, 29);
		frame.getContentPane().add(selectedButton);
	}
	/**
	 * run for running this UI.
	 * */
	public void run() {
		frame.setVisible(true);
		this.frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
	}
	/**
	 * insert data to SQL.
	 * */
	public void insertToSQL(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		if(NO.isSelected())Barcode=((universityComboBox.getSelectedItem()+"").substring(0,1))+(facultyText.getText().substring(0,2))+
				(majorText.getText().substring(0,2))+(subjectText.getText().substring(0,2))+(subjectIDText.getText().substring(0,2));
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();;
			String connectionUrl = "jdbc:mysql://localhost:3306/DataProject";
			String connectionUser = "root";
			String connectionPassword = "";
			conn = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = conn.createStatement();
			System.out.println(report.getBarCode());
			String insertStr1 = "INSERT INTO `tb_"+(universityComboBox.getSelectedItem()+"")+"`(`Barcode`,`Faculty`,`Major`,`SubjectID`,`SubjectName`,`Credit`) VALUES (?, ?, ?, ?, ?, ?)";
			PreparedStatement prepared1 = (PreparedStatement) conn.prepareStatement(insertStr1);
			prepared1.setString(1,Barcode);
			prepared1.setString(2,facultyText.getText());
			prepared1.setString(3,majorText.getText());
			prepared1.setString(4,subjectIDText.getText());
			prepared1.setString(5,subjectText.getText());
			prepared1.setString(6,creditText.getText());
			prepared1.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	}
}
