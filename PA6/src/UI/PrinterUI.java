package UI;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import creditTransfer.Student;

/**
 * Printer is class that show the preview print and can print it
 * 
 * @author kundjanasith
 * @author nattapat
 */
public class PrinterUI implements Runnable{

	/**
	 * frame is JFrame that the frame of this window
	 */
	private JFrame frame;

	/**
	 * textArea is JTextArea that show the preview print
	 */
	private JTextArea textArea;

	/**
	 * scrollPane is JScrollPane that the scroll panel of this textArea
	 */
	private JScrollPane scrollPane;

	/**
	 * student is Student that use this program for transfer credit
	 */
	private Student student;

	/**
	 * printButton is JButton that use for print the report
	 */
	private JButton printButton;

	/**
	 * Constructor of PrinterUI that use for create PrinterUI
	 * @param user is Student that use this program for transfer credit
	 */
	public PrinterUI(Student user) {
		this.student=user;
		initialize();
	}

	/**
	 * initialize is method for initialize value of PrinterUI
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, 691, 416);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 691, 395);
		frame.getContentPane().add(scrollPane);
		String line1 = "Welcome to " + this.student.getTo().getName() + "\n";
		String line2 = "Firstname: " + this.student.getFirstname() + "\n";
		String line3 = "Lastname: " + this.student.getLastname() + "\n";
		String line4 = "ID: " + this.student.getID() + "\n";
		String line5 = "From: \n";
		String line6 = "University: " + this.student.getFrom().getName() + " Faculty: "
				+ this.student.getFrom().getFaculty() + " Major: "
				+ this.student.getFrom().getFaculty() + "\n";
		String line7 = "To: \n";
		String line8 = "University: " + this.student.getTo().getName() + " Faculty: "
				+ this.student.getTo().getFaculty() + " Major: "
				+ this.student.getTo().getFaculty() + "\n";
		String line9 = "Credit Transfer . . .\n";
		String line12 = "";
		for (int i = 0; i < this.student.getToSubjectId().size(); i++) {
			line12 += "| From_SubjectId: " + this.student.getFromSubjectId().get(i);
			line12 += "| To_SubjectId: " + this.student.getToSubjectId().get(i);
			line12 += "| Credit: " + this.student.getCredit().get(i);
			line12 += "| Grade: " + this.student.getGrade().get(i) + " |\n";
		}
		String all1 = line1 + line2 + line3 + line4 + line5 + line6;
		String all2 = line7 + line8 + line9 + line12;
		String all0 = all1 + all2;
		textArea = new JTextArea(all0);
		textArea.setEnabled(false);
		scrollPane.setViewportView(textArea);
		printButton = new JButton("Print");
		printButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					boolean complete = textArea.print();
				} catch (PrinterException error) {
					error.printStackTrace();
				}
				
				frame.setVisible(false);
				MainUI.getFrame().setVisible(true);
				MainUI.getFrame().setLocation(TransferUI.location);
			}
			
		});
		scrollPane.setColumnHeaderView(printButton);
	}

	/**
	 * run is method for show PrinterUI window
	 */
	public void run() {
		try {
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
