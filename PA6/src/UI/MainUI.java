package UI;
import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.net.URL;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.SwingConstants;

import creditTransfer.Searcher;
import creditTransfer.Setting;
import creditTransfer.University;
import UI.SettingUI;
/**
 * @author nattapat
 * @author kundjanasith
 * MainUI is Main Menu of Credit Transfer.
 * */
public class MainUI implements Runnable {
	private static JFrame frame;
	private JLabel bg;
	private JButton settingButton;
	private JButton transferButton;
	private JButton searchButton;
	private JButton addSubjectButton;
	protected static Setting setting;
	private static Point location;
	private StudentInformationUI studentInfoUI;
	private AddSubjectUI addSubjectUI;
	private searchUI search;
	protected static JLabel myUniversityLabel;
	private SettingUI settingUI;
	protected static University to; 
	/**
	 * Constructor of MainUI.
	 * */
	public MainUI (){
		setting = new Setting();
		settingUI = new SettingUI(setting);
		to = setting.getCurrentUnversity();
		this.frame = new JFrame("Credit Transfer");
		this.frame.setBounds(0, 0, 800, 600);
		initComponent();
		location = new Point();
	}
	/**
	 * get the JFrame.
	 * @return frame of this UI.
	 * */
	public static JFrame getFrame(){
		return frame;
	}
	/**
	 * init all Component in this UI.
	 * */
	public void initComponent(){
		this.bg = new JLabel();
		bg.setBounds(0, 0, 800, 600);
		Container contents = this.frame.getContentPane();
		this.transferButton =  new JButton();
		transferButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Transfer...");
				studentInfoUI = new StudentInformationUI(to);
				setCurrentLocation();
				studentInfoUI.getFrame().setLocation(location);
				SwingUtilities.invokeLater(studentInfoUI);
				MainUI.getFrame().setVisible(false);
			}
		});
		this.searchButton =  new JButton();
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				search = new searchUI();
				search.getFrame().setLocation(location);
				setCurrentLocation();
				SwingUtilities.invokeLater(search);
				MainUI.getFrame().setVisible(false);
			}
		});
		this.addSubjectButton =  new JButton();
		addSubjectButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setCurrentLocation();
				addSubjectUI = new AddSubjectUI(setting);
				addSubjectUI.getFrame().setLocation(location);
				SwingUtilities.invokeLater(addSubjectUI);
				MainUI.getFrame().setVisible(false);


			}

		});
		this.settingButton =  new JButton();
		settingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				settingUI.getFrame().setLocation(frame.getLocationOnScreen());
				SwingUtilities.invokeLater(settingUI);
				MainUI.getFrame().setVisible(false);
			}
		});
		contents.setLayout(null);

		settingButton.setBounds(38, 408, 126, 128);
		transferButton.setBounds(520,222,126,128);
		searchButton.setBounds(649,408,126,128);
		addSubjectButton.setBounds(178,222,126,128);

		myUniversityLabel = new JLabel(to.getName());
		myUniversityLabel.setHorizontalAlignment(SwingConstants.CENTER);
		myUniversityLabel.setBounds(178, 95, 468, 128);
		myUniversityLabel.setFont(new Font("Helvetica", Font.PLAIN, 40));
		myUniversityLabel.setForeground(Color.WHITE);;
		contents.add(myUniversityLabel);
		this.initImages();
		contents.add(addSubjectButton);
		contents.add(searchButton);
		contents.add(transferButton);
		contents.add(settingButton);
		contents.add(bg);


	}
	/**
	 * init All images in this UI.
	 * */
	public void initImages(){
		Class loader = this.getClass();
		URL url_bg = loader.getResource("/images/mainMenu.png");
		ImageIcon bgImageIcon  =  new ImageIcon(url_bg);
		this.bg.setIcon(bgImageIcon);

		URL url_settingButton = loader.getResource("/images/setButton.png");
		ImageIcon settingButtionIcon  =  new ImageIcon(url_settingButton);
		this.settingButton.setIcon(settingButtionIcon);

		URL url_addButton = loader.getResource("/images/addButton.png");
		ImageIcon addButtionIcon  =  new ImageIcon(url_addButton);
		this.addSubjectButton.setIcon(addButtionIcon);

		URL url_searchButton = loader.getResource("/images/searchButton.png");
		ImageIcon searchButtionIcon  =  new ImageIcon(url_searchButton);
		this.searchButton.setIcon(searchButtionIcon);

		URL url_transferButton = loader.getResource("/images/transferButton.png");
		ImageIcon transferButtionIcon  =  new ImageIcon(url_transferButton);
		this.transferButton.setIcon(transferButtionIcon);
	}
	/**
	 * set the Current Location of this window.
	 * */
	public void setCurrentLocation(){
		location  = frame.getLocationOnScreen();
	}
	/**
	 * for running this app.
	 * */
	public void run() {
		try {
			this.frame.setVisible(true);
			this.frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
