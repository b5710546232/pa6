package UI;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Point;
import java.net.URL;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.JPanel;

import creditTransfer.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * TransferUI is UI of transfer.
 * @author nattapat
 * @author kundjanasith
 * */
public class TransferUI implements Runnable{
	private JLabel bg;
	private JLabel firstNameLabel;
	private JTextField  firstNameText;
	private JLabel lastNameLabel;
	private JTextField  lastNameText;
	private JLabel IDLabel;
	private JTextField  IDText;
	private JLabel universityLabel;
	private JTextField universityText;
	private JLabel preSubjectIDLabel;
	private JTextField  preSubjectIDText;
	private JLabel postSubjectIDLabel;
	private JTextField  postSubjectIDText;
	private JLabel gradeLabel;
	private JLabel creditLabel;
	private JTextField  creditText;
	private JLabel facultyLabel;
	private JTextField  facultyText;
	private JLabel majorLabel;
	private JTextField  majorText;
	private static JFrame frame;
	private Container contents;
	private static JTable table;
	private static JScrollPane scrollPane;
	private JLabel totalSubjectLabel;
	private JLabel subjectLabel;
	private JLabel creditResultLabel;
	private JLabel totalCreditLabel;
	private JTextField totalCreditText;
	private JTextField totalSubjectText;
	private JButton saveButton;
	private String studentFirstName;
	private String studentLastName;
	private String studentID;
	private University from;
	private University to;
	private JLabel toLabel;
	private JLabel fromLabel;
	private JLabel toUniversity;
	private JLabel toFaculty;
	private JLabel toMajor;
	private JTextField toUniversityText;
	private JComboBox tofacultyBox;
	private JComboBox toMajorBox;
	private JButton confirmButton1;
	private JButton confirmButton2;
	private JButton transferButton;
	private JButton backButton;
	private DefaultComboBoxModel modelMajor;
	private static String[][] dataRow;
	private static String[] Header;
	private Transfer adapter ;
	protected static Point location;
	private Student user;
	private JComboBox gradeCombo;
	private static ImageIcon bgImageIcon;
	private static URL url_bg;
	private Class loader = this.getClass();
	/**
	 *consturctor of TransferUI.
	 *@param user is student for getting the Information.
	 * */
	public TransferUI (Student user){
		this.user=user;
		frame = new JFrame("Credit Transfer");
		frame.setBounds(0, 0, 800, 600);
		contents = frame.getContentPane();
		this.studentFirstName=user.getFirstname();
		this.studentLastName=user.getLastname();
		this.studentID=user.getID();
		this.to=user.getTo();
		this.from=user.getFrom();
		location = frame.getLocation();
		frame.setLocation(StudentInformationUI.location);
		this.frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		initComponent();
	}
	/**
	 *get frame of UI.
	 *@return frame of UI.
	 * */
	public JFrame getFrame(){
		return this.frame;
	}
	/**
	 * init all Component of UI
	 * */
	public void initComponent(){
		this.bg = new JLabel();
		bg.setText("");
		bg.setBounds(0, 0, 800, 578);
		initTable();
		initLabel();
		initButton();
		initBox();
		initText();
		this.initImages();
		contents.add(bg);


	}

	/**
	 * init all JTable of UI
	 * */
	public void initTable(){
		scrollPane = new JScrollPane(null,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(26, 334, 744, 147);

		dataRow = new String [50][6];
		Header = new String []{"From-SubjectID","From-SubjectName","To-SubjectID","To-SubjectName","Credit","Grade"};

		table = new JTable(dataRow,Header);
		table.setGridColor(Color.black);
		table.setBounds(10,20, 300,400);
		scrollPane.setViewportView(table);

		contents.add(scrollPane);
	}
	/**
	 * init all images of UI
	 * */
	public void initImages(){

		url_bg = loader.getResource("/images/TransferBG.png");
	    bgImageIcon  =  new ImageIcon(url_bg);
		this.bg.setIcon(bgImageIcon);

	}
	/**
	 * init all JComboBox of UI
	 * */
	public void initBox(){

		tofacultyBox = new JComboBox(to.getAllFaculty());
		tofacultyBox.setBounds(98,234,170,27);
		contents.add(tofacultyBox);
		toMajorBox = new JComboBox();
		toMajorBox.setBounds(452,238,217,27);
		contents.add(toMajorBox);
		String[] grade = {"C","C+","B","B+","A"};
		gradeCombo = new JComboBox(grade);
		gradeCombo.setBounds(465, 264, 200, 35);
		gradeCombo.setEnabled(false);
		contents.add(gradeCombo);
	}
	/**
	 * init all JLabel of UI
	 * */
	public void initLabel(){
		contents.setLayout(null);
		firstNameLabel = new JLabel("Firstname");
		firstNameLabel.setBounds(26, 97, 77, 35);
		firstNameLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		firstNameLabel.setForeground(Color.WHITE);
		contents.add(firstNameLabel);

		lastNameLabel = new JLabel("Lastname");
		lastNameLabel.setBounds(280, 97, 77, 35);
		lastNameLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		lastNameLabel.setForeground(Color.WHITE);
		contents.add(lastNameLabel);

		IDLabel = new JLabel("Student ID");
		IDLabel.setBounds(534, 97, 217, 35);
		IDLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		IDLabel.setForeground(Color.WHITE);
		contents.add(IDLabel);

		universityLabel = new JLabel("University");
		universityLabel.setBounds(117, 134, 77, 35);
		universityLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		universityLabel.setForeground(Color.WHITE);
		contents.add(universityLabel);

		facultyLabel = new JLabel("Faculty : ");
		facultyLabel.setBounds(26, 169, 77, 35);
		facultyLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		facultyLabel.setForeground(Color.WHITE);
		contents.add(facultyLabel);

		majorLabel = new JLabel("Major : ");
		majorLabel.setBounds(406, 169, 77, 35);
		majorLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		majorLabel.setForeground(Color.WHITE);
		contents.add(majorLabel);



		preSubjectIDLabel = new JLabel("From-Subject ID : ");
		preSubjectIDLabel.setBounds(26, 264, 130, 35);
		preSubjectIDLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		preSubjectIDLabel.setForeground(Color.WHITE);
		contents.add(preSubjectIDLabel);

		gradeLabel = new JLabel("Grade : ");
		gradeLabel.setBounds(407, 264, 77, 35);
		gradeLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		gradeLabel.setForeground(Color.WHITE);
		contents.add(gradeLabel);

		postSubjectIDLabel = new JLabel("To-Subject ID : ");
		postSubjectIDLabel.setBounds(25, 296, 115, 35);
		postSubjectIDLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		postSubjectIDLabel.setForeground(Color.WHITE);
		contents.add(postSubjectIDLabel);

		creditLabel = new JLabel("Credit : ");
		creditLabel.setBounds(406, 296, 77, 35);
		creditLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		creditLabel.setForeground(Color.WHITE);
		contents.add(creditLabel);

		totalSubjectLabel = new JLabel("Total");
		totalSubjectLabel.setBounds(73, 513, 51, 35);
		totalSubjectLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		totalSubjectLabel.setForeground(Color.WHITE);
		contents.add(totalSubjectLabel);

		subjectLabel = new JLabel("Subject");
		subjectLabel.setBounds(252, 513, 51, 35);
		subjectLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		subjectLabel.setForeground(Color.WHITE);
		contents.add(subjectLabel);

		creditResultLabel = new JLabel("Credit");
		creditResultLabel.setBounds(252, 483, 51, 35);
		creditResultLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		creditResultLabel.setForeground(Color.WHITE);
		contents.add(creditResultLabel);

		totalCreditLabel =  new JLabel("Total");
		totalCreditLabel.setBounds(73, 483, 51, 35);
		totalCreditLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		totalCreditLabel.setForeground(Color.WHITE);
		contents.add(totalCreditLabel);

		toLabel = new JLabel("To : ");
		toLabel.setBounds(26, 197, 29, 35);
		toLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		toLabel.setForeground(Color.WHITE);
		contents.add(toLabel);

		fromLabel = new JLabel("From : ");
		fromLabel.setBounds(26, 134, 77, 35);
		fromLabel.setFont(new Font("Helvetica", Font.PLAIN, 15));
		fromLabel.setForeground(Color.WHITE);
		contents.add(fromLabel);


		toUniversity = new JLabel("University");
		toUniversity.setBounds(117, 197, 77, 35);
		toUniversity.setFont(new Font("Helvetica", Font.PLAIN, 15));
		toUniversity.setForeground(Color.WHITE);
		contents.add(toUniversity);
		toFaculty = new JLabel("Faculty");
		toFaculty.setBounds(26, 230, 77, 35); 
		toFaculty.setFont(new Font("Helvetica", Font.PLAIN, 15));
		toFaculty.setForeground(Color.WHITE);
		contents.add(toFaculty);

		toMajor = new JLabel("Major : ");
		toMajor.setBounds(406, 230, 77, 35);
		toMajor.setFont(new Font("Helvetica", Font.PLAIN, 15));
		toMajor.setForeground(Color.WHITE);
		contents.add(toMajor);

	}
	/**
	 * init all JButton of UI.
	 * each Button has ActionListener.
	 * */
	public void initButton(){
		saveButton = new JButton("");
		saveButton.setIcon(new ImageIcon(loader.getResource("/images/saveButton.png")));
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("====================");
				System.out.println("From-University"+from.getName());
				System.out.println("From-Faculty"+from.getFaculty());
				System.out.println("From-Major"+from.getMajor());
				System.out.println("To-University"+to.getName());
				System.out.println("To-Faculty"+to.getFaculty());
				System.out.println("To-Major"+to.getMajor());
				int n = JOptionPane.showConfirmDialog(frame,"Student Indformation have saved already . . .\n"
						+ "Would you like to print Student Information","Alert ! ",JOptionPane.YES_NO_OPTION);
				Saver save = new Saver(user);
				if(n==JOptionPane.YES_NO_OPTION){
					JOptionPane.showMessageDialog(frame, "Preview . . .");
					PrinterUI print = new PrinterUI(user);
					SwingUtilities.invokeLater(print);
					frame.setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(frame, "Back to main menu . . .");
					frame.setVisible(false);
					MainUI.getFrame().setVisible(true);
					MainUI.getFrame().setLocation(frame.getLocation());
				}
			}
		});
		saveButton.setBounds(645, 491, 102, 57);
		contents.add(saveButton);

		confirmButton1 = new JButton("");
		confirmButton1.setIcon(new ImageIcon(loader.getResource("/images/confirmButton.png")));
		confirmButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tofacultyBox.setEnabled(false);
				to.setFaculty(tofacultyBox.getSelectedItem()+"");
				modelMajor = new  DefaultComboBoxModel(to.getAllMajor(tofacultyBox.getSelectedItem()+""));
				toMajorBox.setModel(modelMajor);
				confirmButton2.setEnabled(true);
				confirmButton1.setEnabled(false);
			}
		});
		confirmButton1.setBounds(292, 230, 100, 25);
		contents.add(confirmButton1);
		confirmButton2 = new JButton("");
		confirmButton2.setIcon(new ImageIcon(loader.getResource("/images/confirmButton.png")));
		confirmButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				toMajorBox.setEnabled(false);
				to.setMajor(toMajorBox.getSelectedItem()+"");
				confirmButton2.setEnabled(false);
				transferButton.setEnabled(true);
				preSubjectIDText.setEnabled(true);
				gradeCombo.setEnabled(true);
				adapter = new TransferCredit(from,to);
			}
		});
		confirmButton2.setBounds(677, 230, 100, 25);
		confirmButton2.setEnabled(false);
		contents.add(confirmButton2);

		transferButton = new JButton("");
		transferButton.setIcon(new ImageIcon(loader.getResource("/images/transferButtonclick.png")));
		transferButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("TRAN");
				String toSubjectID = adapter.getTransSubjectId(preSubjectIDText.getText());
				System.out.println(toSubjectID);
				if(toSubjectID==null){
					postSubjectIDText.setText("");
					creditText.setText("");
					JOptionPane.showMessageDialog(frame,"You can not transfer credit for this subject . . .","Warning!!!",JOptionPane.INFORMATION_MESSAGE);
					preSubjectIDText.setText("");
				}
				else if(user.getToSubjectId().contains(toSubjectID)){
					postSubjectIDText.setText("");
					creditText.setText("");
					JOptionPane.showMessageDialog(frame,"This subject you used to transfer . . .","Warning!!!",JOptionPane.INFORMATION_MESSAGE);
					preSubjectIDText.setText("");
				}
				else{
					postSubjectIDText.setText(toSubjectID);
					creditText.setText(to.getCreditFromSubjectID(toSubjectID));
					user.addFormSubjectId(preSubjectIDText.getText());
					user.addFormSubjectName(from.getSubjectNameFromSubjectID(preSubjectIDText.getText()));
					user.addToSubjectId(toSubjectID);
					user.addToSubjectName(to.getSubjectNameFromSubjectID(toSubjectID));
					user.addCredit(to.getCreditFromSubjectID(toSubjectID));
					user.addGrade(gradeCombo.getSelectedItem()+"");
					setTextTable(user);
					totalCreditText.setText(user.getTotalCredit()+"");
					totalSubjectText.setText(user.getTotalSubject()+"");
				}

			}
		});
		transferButton.setBounds(670, 269, 100, 25);
		transferButton.setEnabled(false);
		contents.add(transferButton);
		
		backButton = new JButton("Back to Menu");
		backButton.setBounds(353,502,109,50);
		backButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				MainUI.getFrame().setVisible(true);
				MainUI.getFrame().setLocation(frame.getLocation());
				
			}

		});

	}
	/**
	 * init all JTextField of UI.
	 * */
	public void initText(){
		firstNameText = new JTextField(20);
		firstNameText.setBounds(107, 98, 161, 30);
		firstNameText.setText(this.studentFirstName);
		firstNameText.setEnabled(false);
		contents.add(firstNameText);

		lastNameText = new JTextField(20);
		lastNameText.setBounds(359, 98, 168, 30);
		lastNameText.setText(this.studentLastName);
		lastNameText.setEnabled(false);
		contents.add(lastNameText);

		IDText = new JTextField(20);
		IDText.setBounds(637, 98, 136, 30);
		IDText.setText(this.studentID);
		IDText.setEnabled(false);
		contents.add(IDText);

		universityText = new JTextField(20);
		universityText.setBounds(187, 135, 168, 30);
		universityText.setText(from.getName());
		universityText.setEnabled(false);
		contents.add(universityText);

		facultyText = new JTextField(20); 
		facultyText.setBounds(98, 170, 170, 30);
		facultyText.setText(from.getFaculty());
		facultyText.setEnabled(false);
		contents.add(facultyText);

		majorText = new JTextField(20);
		majorText.setBounds(456, 174, 314, 30);
		majorText.setText(from.getMajor());
		majorText.setEnabled(false);
		contents.add(majorText);


		preSubjectIDText = new JTextField(20);
		preSubjectIDText.setBounds(153, 264, 161, 30);
		preSubjectIDText.setEnabled(false);
		contents.add(preSubjectIDText);

		postSubjectIDText = new JTextField(20);
		postSubjectIDText.setBounds(152, 297, 161, 30);
		postSubjectIDText.setEnabled(false);
		contents.add(postSubjectIDText);

		creditText = new JTextField(20);
		creditText.setBounds(490, 297, 161, 30);
		creditText.setEnabled(false);
		contents.add(creditText);

		totalCreditText = new JTextField(20);
		totalCreditText.setBounds(118, 484, 130, 30);
		totalCreditText.setEnabled(false);
		contents.add(totalCreditText);

		totalSubjectText = new JTextField(20);
		totalSubjectText.setBounds(118, 514, 130, 30);
		totalSubjectText.setEnabled(false);
		contents.add(totalSubjectText);

		toUniversityText = new JTextField(20);;
		toUniversityText.setBounds(187, 198, 170, 30);
		toUniversityText.setText(to.getName());
		toUniversityText.setEnabled(false);
		contents.add(toUniversityText);

	}

	/**
	 * run is for running.
	 * */
	public void run() {
		try {
			TransferUI window = new TransferUI(this.user);
			window.frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * set the Text to JTabler
	 * @param user for getting information.
	 * */
	public void setTextTable(Student user){
		//		table.setValueAt(fromSubjectID,order,0);
		//		table.setValueAt(fromSubjectName,order,1);
		//		table.setValueAt(toSubjectID,order,2);
		//		table.setValueAt(toSubjectName,order,3);
		//		table.setValueAt(credit,order,4);
		//		order++;
		for(int i=0 ; i<user.getCredit().size(); i++){
			table.setValueAt(user.getFromSubjectId().get(i),i,0);
			table.setValueAt(user.getFromSubjectName().get(i),i,1);
			table.setValueAt(user.getToSubjectId().get(i),i,2);
			table.setValueAt(user.getToSubjectName().get(i),i,3);
			table.setValueAt(user.getCredit().get(i),i,4);
			table.setValueAt(user.getGrade().get(i),i,5);
		}
	}
}
