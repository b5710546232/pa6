package UI;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Point;
import java.net.URL;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import creditTransfer.Setting;
import creditTransfer.Student;
import creditTransfer.University;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * StudentInformationUI  is UI for geting the Student Information.
 * @author nattapat
 * @author kundjanasith
 * */
public class StudentInformationUI implements Runnable{
	private static JFrame frame;
	private University to;
	private static Setting all = new Setting();
	private JLabel bg;
	private JLabel firstNameLabel;
	private JLabel lastNameLabel;
	private JLabel IDLabel;
	private JLabel fromLabel;
	private JLabel fromUniversityLabel;
	private JLabel facultyLabel;
	private JLabel majorLabel;
	private JTextField firstNameText;
	private JTextField lastNameText;
	private JTextField IDText;
	private static University from;
	private JComboBox universityComboBox  ;
	private JComboBox facultyComboBox ;
	private JComboBox majorComboBox;
	private JButton confirmButton;
	private DefaultComboBoxModel modelFaculty;
	private DefaultComboBoxModel modelMajor;
	private JButton confirm2Button;
	private JButton con3;
	private JButton clearButton;
	private JButton nextButton;
	private JButton backButton;
	protected static Point location;
	private Container contents;
	private static ImageIcon bgImageIcon;
	private static URL url_bg;
	private Class loader = this.getClass();
	public StudentInformationUI (University to){
		this.frame = new JFrame("Credit Transfer ++ "+to.getName());
		this.frame.setBounds(0, 0, 800, 600);
		this.contents = this.frame.getContentPane();
		this.to = to;
		initComponent();
	}
	/**
	 * get the JFrame
	 * @return frame of this UI.
	 * */
	public JFrame getFrame(){
		return this.frame;
	}
	/**
	 * init all of Component in UI.
	 * */
	private void initComponent(){
		this.initLabel();
		this.initText();
		this.initComboBox();
		this.initButton();
		this.initImages();
		contents.add(bg);
		this.frame.getContentPane().setLayout(null);
	}
	/**
	 * init all of JLabel in UI.
	 * */
	private void initLabel(){
		this.bg = new JLabel();
		bg.setBounds(0, 0, 800, 578);

		firstNameLabel = new JLabel("Firstname :"); 
		firstNameLabel.setFont(new Font("Helvetica", Font.PLAIN, 20));
		firstNameLabel.setForeground(Color.WHITE);;
		firstNameLabel.setBounds(36, 120, 102, 30);

		lastNameLabel = new JLabel("Lastname :"); 
		lastNameLabel.setForeground(Color.WHITE);
		lastNameLabel.setBounds(36, 169, 102, 50);
		lastNameLabel.setFont(new Font("Helvetica", Font.PLAIN, 20));

		IDLabel = new JLabel("ID :"); 
		IDLabel.setForeground(Color.WHITE);
		IDLabel.setFont(new Font("Helvetica", Font.PLAIN, 20));
		IDLabel.setBounds(36, 231, 40, 20);


		fromLabel = new JLabel("From :"); 
		fromLabel.setForeground(Color.WHITE);
		fromLabel.setFont(new Font("Helvetica", Font.PLAIN, 20));
		fromLabel.setBounds(36, 274, 70, 50);


		fromUniversityLabel = new JLabel("University :");
		fromUniversityLabel.setForeground(Color.WHITE);
		fromUniversityLabel.setFont(new Font("Helvetica", Font.PLAIN, 20));
		fromUniversityLabel.setBounds(36, 336, 109, 50);


		facultyLabel = new JLabel("Faculty :");
		facultyLabel.setForeground(Color.WHITE);
		facultyLabel.setFont(new Font("Helvetica", Font.PLAIN, 20));
		facultyLabel.setBounds(36, 398, 129, 40);

		majorLabel = new JLabel("Major :"); ;
		majorLabel.setForeground(Color.WHITE);
		majorLabel.setFont(new Font("Helvetica", Font.PLAIN, 20));
		majorLabel.setBounds(36, 450, 159, 30);

		contents.add(firstNameLabel);
		contents.add(lastNameLabel);
		contents.add(IDLabel);
		contents.add(fromLabel);
		contents.add(fromUniversityLabel);
		contents.add(IDLabel);
		contents.add(majorLabel);
		contents.add(facultyLabel);

	}
	/**
	 * init all of JTextField in UI.
	 * */
	private void initText(){
		firstNameText = new JTextField(20);
		firstNameText.setBounds(150, 121, 312, 30);
		contents.add(firstNameText);

		lastNameText = new JTextField(20);
		lastNameText.setBounds(150, 180, 312, 30);
		contents.add(lastNameText);

		IDText = new JTextField(20);
		IDText.setBounds(150, 222, 312, 30);
		contents.add(IDText);
	}
	/**
	 * init all of images in UI.
	 * */
	private void initImages(){

		url_bg = loader.getResource("/images/StudentInformationBG.png");
		bgImageIcon  =  new ImageIcon(url_bg);
		this.bg.setIcon(bgImageIcon);

	}
	/**
	 * reset all of element in this UI.
	 * */
	private void resetAllElement(){
		confirmButton.setEnabled(true);
		confirm2Button.setEnabled(false);
		con3.setEnabled(false);
		modelFaculty = new DefaultComboBoxModel();
		facultyComboBox.setModel(modelFaculty);
		modelMajor = new DefaultComboBoxModel();
		majorComboBox.setModel(modelMajor);
		universityComboBox.setEnabled(true);
		facultyComboBox.setEnabled(true);
		majorComboBox.setEnabled(true);	
		IDText.setText("");
		firstNameText.setText("");
		lastNameText.setText("");
	}
	/**
	 * init all of JComboBox in UI.
	 * */
	public void initComboBox(){

		ArrayList<String> remain = new ArrayList<String>();
		for(int i= 0 ; i<all.getAllUniversity().size() ; i++){
			if(!all.getAllUniversity().get(i).equalsIgnoreCase(to.getName())) remain.add(all.getAllUniversity().get(i));
		}		
		universityComboBox = new JComboBox(remain.toArray());
		universityComboBox.setBounds(150, 348, 312, 30);
		this.contents.add(universityComboBox);


		from = new University(universityComboBox.getSelectedItem()+"");
		facultyComboBox = new JComboBox();
		facultyComboBox.setBounds(150, 405, 312, 30);
		this.contents.add(facultyComboBox);

		majorComboBox = new JComboBox();
		majorComboBox.setBounds(150, 448, 312, 30);
		this.contents.add(majorComboBox);
	}
	/**
	 * init all of JButton in UI.
	 * each Button has ActionListener
	 * */
	private void initButton(){
		ImageIcon con = new ImageIcon(loader.getResource("/images/confirmButton.png"));
		confirmButton = new JButton("");
		confirmButton.setIcon(con);
		confirmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				from.setFromUniversity(universityComboBox.getSelectedItem()+"");
				modelFaculty = new  DefaultComboBoxModel(from.getAllFaculty());
				facultyComboBox.setModel(modelFaculty);
				universityComboBox.setEnabled(false);
				confirmButton.setEnabled(false);
				confirm2Button.setEnabled(true);
			}
		});
		confirmButton.setBounds(512, 348, 100, 25);
		contents.add(confirmButton);

		confirm2Button = new JButton("");
		confirm2Button.setIcon(con);
		confirm2Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				from.setFromUniversity(universityComboBox.getSelectedItem()+"");
				modelMajor = new  DefaultComboBoxModel(from.getAllMajor(facultyComboBox.getSelectedItem()+""));
				majorComboBox.setModel(modelMajor);
				facultyComboBox.setEnabled(false);
				from.setFaculty(facultyComboBox.getSelectedItem()+"");
				confirm2Button.setEnabled(false);
				con3.setEnabled(true);
			}
		});
		confirm2Button.setBounds(512, 405, 100, 25);
		confirm2Button.setEnabled(false);
		contents.add(confirm2Button);

		con3 = new JButton("");
		con3.setIcon(con);
		con3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				majorComboBox.setEnabled(false);
				from.setMajor(majorComboBox.getSelectedItem()+"");
				con3.setEnabled(false);
			}
		});
		con3.setBounds(512, 448, 100, 25);
		con3.setEnabled(false);
		frame.getContentPane().add(con3);

		nextButton = new JButton("");
		nextButton.setIcon(new ImageIcon(loader.getResource("/images/nextButton.png")));
		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				location = StudentInformationUI.this.frame.getLocation();
				boolean checkText = (!firstNameText.getText().equals("")) &&( !lastNameText.getText().equals("")) && (!IDText.getText().equals(""));
				boolean checkBox = (!confirmButton.isEnabled())&&(!confirm2Button.isEnabled())&&(!con3.isEnabled());
				System.out.println("["+firstNameText.getText()+"]");
				System.out.println("checkText  "+checkText);
				System.out.println("checkBox  "+checkBox);
				if(checkText&&checkBox) {
					System.out.println("GOOO...");
					String[] student = {firstNameText.getText(),lastNameText.getText(),IDText.getText()};
					Student user = new Student(student,to,from);
					TransferUI trans = new TransferUI(user);
					frame.setVisible(false);
					location = frame.getLocation();
					SwingUtilities.invokeLater(trans);

					resetAllElement();

				}
				else JOptionPane.showMessageDialog(frame,"Should in put your information ! ! !", "WARNING!!!",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		nextButton.setBounds(667, 502, 100, 57);
		contents.add(nextButton);

		clearButton = new JButton("");
		clearButton.setIcon(new ImageIcon(loader.getResource("/images/ClearButton.png")));
		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetAllElement();

			}
		});
		clearButton.setBounds(512, 502, 100, 57);
		contents.add(clearButton);

		backButton = new JButton("");
		backButton.setBounds(36,502,125,50);
		backButton.setIcon(new ImageIcon(loader.getResource("/images/BackToMenu.png")));
		backButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				resetAllElement();
				frame.setVisible(false);
				MainUI.getFrame().setVisible(true);
				MainUI.getFrame().setLocation(frame.getLocation());

			}
		});
		contents.add(backButton);
	}
	/**
	 * run is for running this UI.
	 * */
	public  void run() {
		try {
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
