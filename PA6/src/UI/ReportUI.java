package UI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import creditTransfer.Setting;


/**
 * ReportUI is class that show report about subject from every university
 * 
 * @author nattapat
 * @author kundjanasith
 */
public class ReportUI implements Runnable{

	/**
	 * frame is JFrame that the frame of this window
	 */
	private JFrame frame;

	/**
	 * table is JTable that the table for show the detail of each subject
	 */
	private JTable table;

	/**
	 * Header is array of String that is Header of table
	 */
	private static String[] Header = { "University", "Faculty", "Major",
		"SubjectID", "SubjectName", "Credit" };

	/**
	 * setting is Setting that have set by this program
	 */
	private Setting setting;

	/**
	 * barCodeList is ArrayList<String> of barcode of each subject
	 */
	private ArrayList<String> barCodeList;

	/**
	 * universityList is ArrayList<String> of university of each subject
	 */
	private ArrayList<String> universityList;

	/**
	 * facultyList is ArrayList<String> of faculty of each subject
	 */
	private ArrayList<String> facultyList;

	/**
	 * majorList is ArrayList<String> of major of each subject
	 */
	private ArrayList<String> majorList;

	/**
	 * subjectIDList is ArrayList<String> of subjectID of each subject
	 */
	private ArrayList<String> subjectIDList;

	/**
	 * subjectNameList is ArrayList<String> of subjectName of each subject
	 */
	private ArrayList<String> subjectNameList;

	/**
	 * creditList is ArrayList<String> of credit of each subject
	 */
	private ArrayList<String> creditsList;

	/**
	 * run is method for show PrinterUI window
	 */
	public void run() {
		frame.setVisible(true);
	}

	/**
	 * Constructor of ReportUI that use for create ReportUI
	 * @param setting is Setting that have set by this program
	 */
	public ReportUI(Setting setting) {
		System.out.println(setting.getAllUniversity().toString());
		this.universityList = new ArrayList<String>();
		this.barCodeList = new ArrayList<String>();
		this.facultyList = new ArrayList<String>();
		this.majorList = new ArrayList<String>();
		this.subjectIDList = new ArrayList<String>();
		this.subjectNameList = new ArrayList<String>();
		this.creditsList = new ArrayList<String>();
		this.setting = setting;
		initialize();
	}

	/**
	 * initialize is method for initialize value of ReportUI
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 900, 300);
		frame.getContentPane().setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 894, 278);
		frame.getContentPane().add(scrollPane);
		for (int i = 0; i < this.setting.getAllUniversity().size(); i++) {
			addSQL(this.setting.getAllUniversity().get(i));
		}
		String[][] dataRow = new String[this.universityList.size()][Header.length];
		table = new JTable(dataRow, Header);
		table.setGridColor(Color.black);
        table.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				setTextTable();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				setTextTable();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				setTextTable();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				setTextTable();
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				setTextTable();
			}
        	
        });
       
		scrollPane.setViewportView(table);
		setTextTable();

	}

	/**
	 * setTextTable is method for set text in table all of cells
	 */
	private void setTextTable() {
		for (int i = 0; i < this.universityList.size(); i++) {
			table.setValueAt(this.universityList.get(i), i, 0);
			table.setValueAt(this.facultyList.get(i), i, 1);
			table.setValueAt(this.majorList.get(i), i, 2);
			table.setValueAt(this.subjectIDList.get(i), i, 3);
			table.setValueAt(this.subjectNameList.get(i), i, 4);
			table.setValueAt(this.creditsList.get(i), i, 5);
		}
	}

	/**
	 * addSQL is method for get all subjects in each university
	 * @param name is String that name of University
	 */
	private void addSQL(String name) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			;
			String connectionUrl = "jdbc:mysql://localhost:3306/DataProject";
			String connectionUser = "root";
			String connectionPassword = "";
			conn = DriverManager.getConnection(connectionUrl, connectionUser,
					connectionPassword);
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM `tb_" + name + "`");
			while (rs.next()) {
				String key = rs.getString("Barcode");
				String fac = rs.getString("Faculty");
				String maj = rs.getString("Major");
				String subId = rs.getString("SubjectID");
				String subName = rs.getString("SubjectName");
				String cre = rs.getString("Credit");
				this.universityList.add(name);
				this.barCodeList.add(key);
				this.facultyList.add(fac);
				this.majorList.add(maj);
				this.subjectIDList.add(subId);
				this.subjectNameList.add(subName);
				this.creditsList.add(cre);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * getBarCode is method for get barcode of subject that selected
	 * @return barcode of subject that selected
	 */
	public String getBarCode() {
		if (table.getSelectedRow() < 0)
			return null;
		return this.barCodeList.get(table.getSelectedRow());
	}

	/**
	 * getUniversity is method for get university of subject that selected
	 * @return university of subject that selected
	 */
	public String getUniversity() {
		if (table.getSelectedRow() < 0)
			return null;
		return this.universityList.get(table.getSelectedRow());
	}

	/**
	 * getFaculty is method for get faculty of subject that selected
	 * @return faculty of subject that selected
	 */
	public String getFaculty() {
		if (table.getSelectedRow() < 0)
			return null;
		return this.facultyList.get(table.getSelectedRow());
	}

	/**
	 * getMajor is method for get major of subject that selected
	 * @return major of subject that selected
	 */
	public String getMajor() {
		if (table.getSelectedRow() < 0)
			return null;
		return this.majorList.get(table.getSelectedRow());
	}

	/**
	 * getSubjectId is method for get subject-id of subject that selected
	 * @return subject-id of subject that selected
	 */
	public String getSubjectId() {
		if (table.getSelectedRow() < 0)
			return null;
		return this.subjectIDList.get(table.getSelectedRow());
	}

	/**
	 * getSubjectName is method for get subject-name of subject that selected
	 * @return subject-name of subject that selected
	 */
	public String getSubjectName() {
		if (table.getSelectedRow() < 0)
			return null;
		return this.subjectNameList.get(table.getSelectedRow());
	}

	/**
	 * getCredit is method for get credit of subject that selected
	 * @return credit of subject that selected
	 */
	public String getCredit() {
		if (table.getSelectedRow() < 0)
			return null;
		return this.creditsList.get(table.getSelectedRow());
	}

}
